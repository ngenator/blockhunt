;BlockHunt Definitions

.686P		; Pentium Pro or later
.MODEL flat, stdcall
.STACK 4096

OPTION casemap :none

INCLUDE masm32\include\windows.inc
INCLUDE masm32\include\kernel32.inc
INCLUDE masm32\include\user32.inc
INCLUDE masm32\include\masm32.inc
INCLUDE masm32\macros.asm

INCLUDELIB masm32\lib\kernel32.lib
INCLUDELIB masm32\lib\user32.lib
INCLUDELIB masm32\lib\masm32.lib

;------------------------------------------------------------
; Constants
;------------------------------------------------------------

CONSOLE_WIDTH = 130
CONSOLE_HEIGHT = 65

DESIRED_LEVELS = 10
DIFFICULTY = 5 ; The difficulty, which is used to calculate how many blocks per level

MIN_BLOCK_WIDTH = 3
MAX_BLOCK_WIDTH = 7
MIN_BLOCK_HEIGHT = 2
MAX_BLOCK_HEIGHT = 5
BLOCK_PADDING = 2 ; Minimum spacing between blocks

CONSOLE_WIDTH_PADDING  = 1
CONSOLE_HEIGHT_PADDING = 1

BASE_TIMER = 5 ; The initial time to start at, we add the level to this

;These should not be changed
MAX_BLOCKS = DESIRED_LEVELS * DIFFICULTY
TIMER_RATE = 1000 ; The ammount of time to wait before decrementing the counter
POLLING_RATE = 50 ; The ammount of time to wait between draw
DISPLAY_PADDING = 8 ; Padding for the timer and score so we don't write over them

;------------------------------------------------------------
; Structs
;------------------------------------------------------------

BLOCK STRUCT
	id BYTE 0 ;Block id
	dbClicked BYTE 0 ;Was the block clicked?
	dwPos COORD <> ;Location of the top left of the block
	dwSize COORD <> ;Size of the block: x = width, y = height
BLOCK ENDS

LEVEL STRUCT
	id WORD ? ;Level id
	nClicked WORD ? ;Number of blocks clicked (not used)
	nBlocks  WORD ? ;Number of desired blocks
	nBlocksCount WORD ? ;Number of blocks generated
	pBlocks BLOCK MAX_BLOCKS DUP(<>)
LEVEL ENDS

GAME STRUCT
	nPollingTickCount DWORD ? ;Polling tick counter
	nTimerTickCount DWORD ? ;Tick counter
	nTotalScore WORD ? ;Total score
	nLevel WORD ? ;Current level
	nTimer WORD ? ;Remaining time
	pLevel LEVEL <>
GAME ENDS

MENU STRUCT
	pBlocks BLOCK MAX_BLOCKS DUP(<>)
MENU ENDS

;------------------------------------------------------------
; Procedures
;------------------------------------------------------------
ConsoleInit PROTO
UpdateOutput PROTO
ClearOutput PROTO

DrawIntro PROTO, :HANDLE, :HANDLE
DrawBlock PROTO, :DWORD, :BLOCK
RemoveBlock PROTO, :DWORD, :BLOCK
IsCoordInBlock PROTO, :COORD, :BLOCK
IsBlockInBlock PROTO, :BLOCK, :BLOCK
IsBlockInLevel PROTO, :BLOCK
IsCoordInLevel PROTO, :COORD
DoLevel PROTO, :HANDLE, :WORD
IsLevelComplete PROTO 
RemoveBlockFromLevel PROTO, :HANDLE, :BYTE
GetClickedBlock PROTO, :HANDLE
ClickToContinue PROTO, :HANDLE, :HANDLE, :WORD
RandomRange PROTO, :WORD, :WORD

DrawTimer PROTO, :DWORD
PrintNumber PROTO, :WORD, :BYTE,:DWORD
DrawOutro PROTO, :HANDLE

;------------------------------------------------------------
; Macros
;------------------------------------------------------------

;Return a value from a proc by moving it to eax
Return MACRO value:REQ
	mov eax,value
	ret
ENDM

.data
	pBlankChars BYTE CONSOLE_HEIGHT*CONSOLE_WIDTH DUP(0)

.data?
	pGame GAME <> ; This holds all of the game information