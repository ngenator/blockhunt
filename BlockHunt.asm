TITLE BlockHunt Procedures

INCLUDE BlockHunt.inc

.code

;------------------------------------------------------------
DrawIntro PROC USES ebx ecx esi, 
	hConsoleInput:HANDLE, hConsoleOutput:HANDLE	
; Draws the intro screen to the provided output handle
;------------------------------------------------------------
	LOCAL pCursorPos:COORD, iCount:DWORD

.data
	pBitMapBlock	BYTE 219,219,219,0  ,0  ,0  ,219,0  ,0  ,0  ,0  ,0  ,0  ,219,219,0  ,0  ,0  ,0  ,219,219,0  ,0  ,0  ,219,0  ,0  ,219 
					BYTE 219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219  
					BYTE 219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,0  ,0  ,219,0  ,219,0   
					BYTE 219,219,219,0  ,0  ,0  ,219,0  ,0  ,0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,0  ,0  ,219,219,0  ,0   
					BYTE 219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,0  ,0  ,219,0  ,219,0   
					BYTE 219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219 
					BYTE 219,219,219,0  ,0  ,0  ,219,219,219,219,0  ,0  ,0  ,219,219,0  ,0  ,0  ,0  ,219,219,0  ,0  ,0  ,219,0  ,0  ,219
	iBitMapBlockSize = ($ - pBitMapBlock)
	iBitMapBlockColumns = LENGTHOF pBitMapBlock
	iBitMapBlockRows = iBitMapBlockSize / iBitMapBlockColumns

	pBitMapHunt		BYTE 0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,219,219,219
					BYTE 0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,219,0  ,219,0  ,0  ,0  ,222,221,0
					BYTE 0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,219,0  ,219,0  ,0  ,0  ,222,221,0
					BYTE 0  ,0  ,0  ,219,219,219,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,219,219,0  ,0  ,0  ,222,221,0
					BYTE 0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,219,219,0  ,0  ,0  ,222,221,0
					BYTE 0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,222,221,0
					BYTE 0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,219,219,0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,222,221,0
	iBitMapHuntSize = ($ - pBitMapHunt)
	iBitMapHuntColumns = LENGTHOF pBitMapHunt
	iBitMapHuntRows = iBitMapHuntSize / iBitMapHuntColumns

	pInstructions BYTE '                                  INSTRUCTIONS:                                  '
	              BYTE '                                                                                 '
	              BYTE 'This game is divided into levels with an increasing number of blocks.            '
	              BYTE 'Your goal is to click on as many blocks as possible before the time runs out.    '
				  BYTE 'Clicking a block removes it from the level, each block can be clicked only once. '
	              BYTE 'Each block is worth 1 point. If you click all of the blocks before time runs out,'
	              BYTE 'you will be awarded bonus points for each second remaining.                      '
				  BYTE 'Between levels, the game will pause and ask you to press a button when ready.    '
				  BYTE '                                                                                 '
	              BYTE 'Your score is in the top left, the time remaining is in the top right.           '
	              BYTE 'Good luck!                                                                       '
	iInstructionsSize = ($ - pInstructions)
	iInstructionsLength = LENGTHOF pInstructions
	iInstructionsCount = iInstructionsSize / iInstructionsLength
		
.code
	;Dynamically calculate the position to start drawing, centers it in the middle of the window
	mov ax,CONSOLE_WIDTH/2 ;Half of the console width
	sub ax,iBitMapBlockColumns/2 ;Subtract half of the bitmap width so that the middle of the bitmap is at half console width (center)
	mov [pCursorPos.x],ax
	mov [pCursorPos.y],2

	mov esi,0
	mov ecx,[iBitMapBlockRows]
PrintBitMapBlock:
	mov ebx,OFFSET [pBitMapBlock]

	push ecx
	INVOKE SetConsoleCursorPosition, hConsoleOutput, DWORD PTR pCursorPos
	INVOKE WriteConsole, hConsoleOutput, ADDR [ebx + esi], iBitMapBlockColumns, ADDR iCount, NULL
	pop ecx
		
	add esi,iBitMapBlockColumns ;Increase the offset by the number of columns (which will be the start of the next row)
	inc pCursorPos.y
	loop PrintBitMapBlock

	inc pCursorPos.y ;Add space between words

	mov esi,0
	mov ecx,[iBitMapHuntRows]
PrintBitMapHunt:
	mov ebx,OFFSET [pBitMapHunt]

	push ecx
	INVOKE SetConsoleCursorPosition, hConsoleOutput, DWORD PTR pCursorPos
	INVOKE WriteConsole, hConsoleOutput, ADDR [ebx + esi], iBitMapHuntColumns, ADDR iCount, NULL
	pop ecx
		
	add esi,iBitMapHuntColumns ;Increase the offset by the number of columns (which will be the start of the next row)
	inc pCursorPos.y
	loop PrintBitMapHunt

	; Center the cursor based on the longest line
	mov ax,iBitMapBlockColumns/2
	add pCursorPos.x,ax 
	sub pCursorPos.x,iInstructionsLength/2
	add pCursorPos.y,5

	mov esi,0
	mov ecx,iInstructionsCount
PrintInstructions:
	mov ebx,OFFSET [pInstructions]

	push ecx
	INVOKE SetConsoleCursorPosition, hConsoleOutput, DWORD PTR pCursorPos
	INVOKE WriteConsole, hConsoleOutput, ADDR [ebx + esi], iInstructionsLength, ADDR iCount, NULL
	pop ecx
		
	add esi,iInstructionsLength
	inc pCursorPos.y
	loop PrintInstructions

	INVOKE ClickToContinue, hConsoleInput, hConsoleOutput, 10

	Return 0
DrawIntro ENDP

;------------------------------------------------------------
DrawBlock PROC USES ecx,
	m_hConsoleOut:DWORD, m_hBlock:BLOCK
; Draws a block to the provided output handle
;------------------------------------------------------------
	LOCAL nChars:WORD, nLines:WORD, nCount:DWORD

.data
	pCharBuffer BYTE CONSOLE_WIDTH DUP(219)

.code
	mov ax,m_hBlock.dwSize.x
	mov [nChars],ax
	mov ax,m_hBlock.dwSize.y
	mov [nLines],ax
	
	;Do the drawing
	movzx ecx,[nLines]
DrawBlock_DoDraw:
	push ecx ;I think one of these was messing with ecx, so save before calling and restore after

	;http://msdn.microsoft.com/en-us/library/windows/desktop/ms686025(v=vs.85).aspx
	INVOKE SetConsoleCursorPosition,
		m_hConsoleOut,
		DWORD PTR m_hBlock.dwPos

	;http://msdn.microsoft.com/en-us/library/windows/desktop/ms687401(v=vs.85).aspx
	INVOKE WriteConsole,
		m_hConsoleOut,
		ADDR pCharBuffer,	; pointer to char buffer
		nChars,				; number of chars to write
		ADDR nCount,		; output count
		NULL				; reserved, must be NULL

	inc m_hBlock.dwPos.y

	pop ecx
	loop DrawBlock_DoDraw

	Return 1
DrawBlock ENDP

;------------------------------------------------------------
RemoveBlock PROC USES ecx,
	m_hConsoleOut:DWORD, m_hBlock:BLOCK
	LOCAL nChars:WORD, nLines:WORD, nCount:DWORD
; Removes a block by overwriting it with blank characters
;------------------------------------------------------------

	mov ax,m_hBlock.dwSize.x
	mov [nChars],ax
	mov ax,m_hBlock.dwSize.y
	mov [nLines],ax
	
	;Do the drawing
	movzx ecx,[nLines]
Draw:
	push ecx

	INVOKE SetConsoleCursorPosition, m_hConsoleOut, DWORD PTR m_hBlock.dwPos
	INVOKE WriteConsole, m_hConsoleOut, ADDR pBlankChars, nChars, ADDR nCount, NULL
	CALL UpdateOutput

	inc m_hBlock.dwPos.y

	pop ecx
	loop Draw

	Return 1
RemoveBlock ENDP

;------------------------------------------------------------
IsCoordInBlock PROC USES ebx ecx edx,
	hPos:COORD, hBlock:BLOCK
;Checks if the coord is in the block
;------------------------------------------------------------
	
	mov bx,hPos.x
	mov cx,hPos.y

	mov dx,hBlock.dwPos.x
	cmp bx,dx
	jl NotIn						;jump if less than

	add dx,hBlock.dwSize.x
	dec dx
	cmp bx,dx
	jg NotIn						;jump if greater than

	mov dx,hBlock.dwPos.y
	cmp cx,dx
	jl NotIn						;jump if less than

	add dx,hBlock.dwSize.y
	dec dx
	cmp cx,dx
	jg NotIn						;jump if greater than

	jmp IsIn

NotIn:
	Return 0
IsIn:
	Return 1
IsCoordInBlock ENDP

;------------------------------------------------------------
IsBlockInBlock PROC USES ecx edx,
        hBlock:BLOCK, hOtherBlock:BLOCK
; Checks if one block overlaps the other block
; Follows this format:
;   A.X1 < B.X2
;   A.X2 > B.X1
;   A.Y1 < B.Y2
;   A.Y2 > B.Y1
; If all are true, they intersect
;------------------------------------------------------------
	CheckRight:
        mov dx,hBlock.dwPos.x
		sub dx,BLOCK_PADDING
        mov cx,hOtherBlock.dwPos.x
        add cx,hOtherBlock.dwSize.x
        cmp dx,cx
		jl CheckLeft

		jmp NotIn

	CheckLeft:
		mov dx,hBlock.dwPos.x
		add dx,hBlock.dwSize.x
		mov cx,hOtherBlock.dwPos.x
		sub cx,BLOCK_PADDING
		cmp dx,cx
		jg CheckBelow

		jmp NotIn

	CheckBelow:
		mov dx,hBlock.dwPos.y
		sub dx,BLOCK_PADDING
		mov cx,hOtherBlock.dwPos.y
		add cx,hOtherBlock.dwSize.y
		cmp dx,cx
		jl CheckAbove

		jmp NotIn
		
	CheckAbove:
		mov dx,hBlock.dwPos.y
		add dx,hBlock.dwSize.y
		mov cx,hOtherBlock.dwPos.y
		sub cx,BLOCK_PADDING
		cmp dx,cx
		jg IsIn

		jmp NotIn

	IsIn:
        Return 1
	NotIn:
		Return 0
IsBlockInBlock ENDP

;------------------------------------------------------------
IsBlockInLevel PROC USES ebx ecx edx esi,
	hBlock:BLOCK
;Checks if the block is overlapping any other blocks in the level
;------------------------------------------------------------

	mov esi,OFFSET (LEVEL PTR [pGame.pLevel]).pBlocks
	movzx ecx,(LEVEL PTR [pGame.pLevel]).nBlocksCount
	cmp ecx,0 ; Make sure that there are blocks, use current blocks number incase we havent generated every block for the level yet
	jle NotIn 
Check:
	push ecx

	INVOKE IsBlockInBlock, hBlock, BLOCK PTR [esi]
	cmp eax,1
	je IsIn

	add esi,TYPE BLOCK	
	pop ecx
	loop Check

NotIn:
	Return 0
IsIn:
	pop ecx ;CLEANUP THE STACK SINCE WE EXITED THE LOOP BEFORE POPPING ECX
	Return DWORD PTR [(BLOCK PTR [esi]).id]
IsBlockInLevel ENDP

;------------------------------------------------------------
IsCoordInLevel PROC USES ebx ecx edx,
	hCoord:COORD
;Checks if the coord is in one of the blocks in the level
;------------------------------------------------------------

	mov esi,OFFSET (LEVEL PTR [pGame.pLevel]).pBlocks
	movzx ecx,(LEVEL PTR [pGame.pLevel]).nBlocks
Check:
	;Check if the block has been clicked yet
	cmp (BLOCK PTR [esi]).dbClicked,1
	jge Continue

	INVOKE IsCoordInBlock, hCoord, BLOCK PTR [esi]
	cmp eax,1
	je IsIn

Continue:
	add esi,TYPE BLOCK
	loop Check

NotIn:
	Return 0
IsIn:
	;The coord is in the level, set clicked to 1 and return the id of the block that was clicked in eax
	mov (BLOCK PTR [esi]).dbClicked,1
	Return DWORD PTR [(BLOCK PTR [esi]).id]
IsCoordInLevel ENDP

;------------------------------------------------------------
DoLevel PROC USES ebx ecx edx esi edi,
	m_hConsoleOutput:HANDLE, nLevelId:WORD
;Generates a new level and draws it to the provided output buffer
;------------------------------------------------------------

	;Prepare to generate the level
	mov (LEVEL PTR [pGame.pLevel]).nClicked,0
	mov ax,nLevelId
	mov (LEVEL PTR [pGame.pLevel]).id,ax
	mov bx,DIFFICULTY
	mul bx ;ax = ax * bx

	;Make sure we don't get too greedy, must be less than or equal to MAX_BLOCKS
	cmp ax,MAX_BLOCKS
	jg OutOfBounds

	mov (LEVEL PTR [pGame.pLevel]).nBlocks,ax
	mov (LEVEL PTR [pGame.pLevel]).nBlocksCount,0

	mov edi,OFFSET (LEVEL PTR [pGame.pLevel]).pBlocks
	movzx ecx,[(LEVEL PTR [pGame.pLevel]).nBlocks]
GenerateBlocks:
	push ecx

	;Generate a block to be added to the level
Regenerate:
	mov ax,(LEVEL PTR [pGame.pLevel]).nBlocksCount
	mov (BLOCK PTR [edi]).id,al
	mov (BLOCK PTR [edi]).dbClicked,0
	INVOKE RandomRange, MIN_BLOCK_WIDTH, MAX_BLOCK_WIDTH
	mov (BLOCK PTR [edi]).dwSize.x,ax
	INVOKE RandomRange, MIN_BLOCK_HEIGHT, MAX_BLOCK_HEIGHT
	mov (BLOCK PTR [edi]).dwSize.y,ax
	INVOKE RandomRange, CONSOLE_WIDTH_PADDING, CONSOLE_WIDTH - CONSOLE_WIDTH_PADDING - MAX_BLOCK_WIDTH
	mov (BLOCK PTR [edi]).dwPos.x,ax
	INVOKE RandomRange, CONSOLE_HEIGHT_PADDING + DISPLAY_PADDING, CONSOLE_HEIGHT - CONSOLE_HEIGHT_PADDING - MAX_BLOCK_HEIGHT
	mov (BLOCK PTR [edi]).dwPos.y,ax

	;Check to make sure the generated block doe not overlap any other blocks
	INVOKE IsBlockInLevel, BLOCK PTR [edi]
	cmp eax,0
	;If does overlap, generate new block and check again
	jg Regenerate
	
	add edi,TYPE BLOCK
	inc (LEVEL PTR [pGame.pLevel]).nBlocksCount
	pop ecx
	loop GenerateBlocks

	;Display the level
	mov esi,OFFSET (LEVEL PTR [pGame.pLevel]).pBlocks
	movzx ecx,[(LEVEL PTR [pGame.pLevel]).nBlocks]
DrawBlocks:	
	push ecx ;Store ecx for outer loop

	INVOKE DrawBlock, m_hConsoleOutput, BLOCK PTR [esi]

	add esi,TYPE BLOCK
	pop ecx ;Restore ecx for outer loop
	loop DrawBlocks

	Return 1
OutOfBounds:
	Return 0
DoLevel ENDP

;------------------------------------------------------------
IsLevelComplete PROC 
; Checks if the user has completed the level (clicked all of the blocks)
;------------------------------------------------------------
	mov ax,(LEVEL PTR [pGame.pLevel]).nClicked
	cmp ax,0
	jle NotComplete
	cmp ax,(LEVEL PTR [pGame.pLevel]).nBlocks
	jge Complete

NotComplete:
	Return 0
Complete:
	Return 1
IsLevelComplete ENDP

;------------------------------------------------------------
RemoveBlockFromLevel PROC,
	m_hConsoleOutput:HANDLE, nBlockId:BYTE
;Removes a block from the level by id (just overwrites it)
;------------------------------------------------------------

	mov esi,OFFSET (LEVEL PTR [pGame.pLevel]).pBlocks
	movzx ecx,(LEVEL PTR [pGame.pLevel]).nBlocks
Check:
	push ecx
	mov al,(BLOCK PTR [esi]).id
	cmp al,nBlockId
	jne Continue

	INVOKE RemoveBlock, m_hConsoleOutput, BLOCK PTR [esi]

Continue:
	add esi,TYPE BLOCK
	pop ecx	
	loop Check

Done:
	Return 0
RemoveBlockFromLevel ENDP

;------------------------------------------------------------
GetClickedBlock PROC USES ebx ecx,
	hConsoleInput:HANDLE
;Reads the console input buffer looking for a mouse left click event
;If there is one, it checks whether the click occurred inside one of the blocks in the level
;and returns the id of the block that was clicked or 0
;------------------------------------------------------------
	LOCAL evBuffer:INPUT_RECORD, evEvents:DWORD

Peek: ; Peek to see if we have a pending event. If so, read it.
	INVOKE PeekConsoleInput, hConsoleInput, ADDR evBuffer, 1, ADDR evEvents
	test evEvents,0FFFFh
	jz Done ;No pending events, nothing to do

	INVOKE ReadConsoleInput, hConsoleInput, ADDR evBuffer, 1, ADDR evEvents

	cmp evBuffer.EventType, MOUSE_EVENT
	jne Done ;Not mouse event, nothing to do

	cmp evBuffer.MouseEvent.dwEventFlags,0002h
	jg Done ;Not double click, nothing to do
	cmp evBuffer.MouseEvent.dwEventFlags,0
	jne Done ;Not button press, nothing to do

	cmp evBuffer.MouseEvent.dwButtonState,00001h
	jl Done ;Not left click, nothing to do
	cmp evBuffer.MouseEvent.dwButtonState,00002h
	jg Done ;Not right click, nothing to do

	;Check if the mouse click occurred inside a block in the level
	INVOKE IsCoordInLevel,evBuffer.MouseEvent.dwMousePosition
	cmp eax,0
	je Done ;Not in level

	inc (LEVEL PTR [pGame.pLevel]).nClicked

	Ret ;Leave eax alone, block id is returned in eax
Done:
	Return 0
GetClickedBlock ENDP

;------------------------------------------------------------
ClickToContinue PROC USES ebx ecx,
	hConsoleInput:HANDLE, hConsoleOutput:HANDLE, nVerticalOffset:WORD
; Returns 1 when the user clicks the block, accepts left and right clicks
;------------------------------------------------------------
	LOCAL evBuffer:INPUT_RECORD, evEvents:DWORD, pCursorPos:COORD, iCount:DWORD

.data
	pClickToContinue BYTE "Click Here to Continue",0
	nXPos = (CONSOLE_WIDTH/2) - (LENGTHOF pClickToContinue)
	nYPos = CONSOLE_HEIGHT/2 - 4
	nWidth = LENGTHOF pClickToContinue*2
	nHeight = 9
	pContinueBlock BLOCK <0,0,<nXPos,nYPos>,<nWidth,nHeight>>
	pContinueBlockRemove BLOCK <0,0,<nXPos+3,nYPos+2>,<nWidth-6,nHeight-4>>
.code
	mov ax,nVerticalOffset
	add pContinueBlock.dwPos.y,ax
	add pContinueBlockRemove.dwPos.y,ax
	
	INVOKE DrawBlock, hConsoleOutput, pContinueBlock
	INVOKE RemoveBlock, hConsoleOutput, pContinueBlockRemove

	mov ax,pContinueBlock.dwPos.x
	add ax,LENGTHOF pClickToContinue/2
	mov pCursorPos.x,ax
	mov ax,pContinueBlock.dwPos.y
	add ax,4
	mov pCursorPos.y,ax

	INVOKE SetConsoleCursorPosition, hConsoleOutput, DWORD PTR pCursorPos
	INVOKE WriteConsole, hConsoleOutput, ADDR pClickToContinue, LENGTHOF pClickToContinue, ADDR iCount, NULL

	CALL UpdateOutput

Peek: ; Peek to see if we have a pending event. If so, read it.
	INVOKE Sleep, 5 ; Sleep so we don't eat up cpu
	INVOKE PeekConsoleInput, hConsoleInput, ADDR evBuffer, 1, ADDR evEvents
	test evEvents,0FFFFh
	jz Peek ;No pending events, nothing to do

	INVOKE ReadConsoleInput, hConsoleInput, ADDR evBuffer, 1, ADDR evEvents

	cmp evBuffer.EventType, MOUSE_EVENT
	jne Peek ;Not mouse event, nothing to do

	cmp evBuffer.MouseEvent.dwEventFlags,0002h
	jg Peek ;Not double click, nothing to do
	cmp evBuffer.MouseEvent.dwEventFlags,0
	jne Peek ;Not button press, nothing to do

	cmp evBuffer.MouseEvent.dwButtonState,00001h
	jl Peek ;Not left click, nothing to do
	cmp evBuffer.MouseEvent.dwButtonState,00002h
	jg Peek ;Not right click, nothing to do

	;Check if the mouse click occurred inside the block
	INVOKE IsCoordInBlock,evBuffer.MouseEvent.dwMousePosition,pContinueBlock
	cmp eax,0
	je Peek ;Not in block

	INVOKE RemoveBlock, hConsoleOutput, pContinueBlock

	Return 1
ClickToContinue ENDP

;------------------------------------------------------------
RandomRange PROC,
	lower:WORD, upper:WORD
; Generates a rendom number between the lower and upper values provided
;------------------------------------------------------------
	mov eax,0
	mov ax,upper
	sub ax,lower
	INVOKE nrandom, eax
	add ax,lower
	ret
RandomRange ENDP

DrawTimer PROC USES eax ebx ecx esi, m_hConsoleOutput:DWORD
.data
	timerAt dd 11 ;check time will decrease initially / start at 10
	;;all bitmaps 6 x 7
	
.code
	checkTime: dec timerAt
		   cmp timerAt,10
		   jne CheckNine
			 INVOKE PrintNumber,10,1,m_hConsoleOutput
			 jmp After

	CheckNine: cmp timerAt,9
		   jne CheckEight
		     INVOKE PrintNumber,9,1,m_hConsoleOutput
			 jmp After

	CheckEight:cmp timerAt,8
           jne CheckSeven
		     INVOKE PrintNumber,8,1,m_hConsoleOutput
			 jmp After

	CheckSeven:cmp timerAt,7
           jne CheckSix
		     INVOKE PrintNumber,7,1,m_hConsoleOutput
			 jmp After

	CheckSix:  cmp timerAt,6
           jne CheckFive
		     INVOKE PrintNumber,6,1,m_hConsoleOutput
			 jmp After

	CheckFive: cmp timerAt,5
           jne CheckFour
		     INVOKE PrintNumber,5,1,m_hConsoleOutput
			 jmp After

	CheckFour: cmp timerAt,4
           jne CheckThree
		     INVOKE PrintNumber,4,1,m_hConsoleOutput
			 jmp After

	CheckThree:cmp timerAt,3
		   jne CheckTwo
		     INVOKE PrintNumber,3,1,m_hConsoleOutput
			 jmp After

	CheckTwo:  cmp timerAt,2
		   jne CheckOne
		     INVOKE PrintNumber,2,1,m_hConsoleOutput
			 jmp After

	CheckOne:  cmp timerAt,1
		   jne IsZero
		    INVOKE PrintNumber,1,1,m_hConsoleOutput
			 jmp After


	IsZero:	    INVOKE PrintNumber,0,1,m_hConsoleOutput
		   mov eax,11 ;just to start the loop again
		   mov timerAt,eax
After: Return 0
DrawTimer ENDP

PrintNumber PROC USES eax ebx edx ecx esi, numToPrint:WORD, isTimer:BYTE, m_hConsoleOutput:DWORD
            LOCAL pCursorPos:COORD, iCount:DWORD
.data
    iBitMapColumns=6
	iBitMapRows=7

	pBitMapZero   	BYTE 219,219,219,219,219,219  
					BYTE 219,0  ,  0,0  ,0  ,219  
					BYTE 219,0  ,  0,0  ,0  ,219  
					BYTE 219,0  ,  0,0  ,0  ,219  
					BYTE 219,0  ,  0,0  ,0  ,219  
					BYTE 219,0  ,  0,0  ,0  ,219  
					BYTE 219,219,219,219,219,219 					
	iBitMapZeroSize	= ($ - pBitMapZero)
	pBitMapOne   	BYTE 0  ,0  , 0,0  ,0  ,219  
					BYTE 0  ,0  , 0,0  ,0  ,219  
					BYTE 0  ,0  , 0,0  ,0  ,219  
					BYTE 0  ,0  , 0,0  ,0  ,219  
					BYTE 0  ,0  , 0,0  ,0  ,219  
					BYTE 0  ,0  , 0,0  ,0  ,219
					BYTE 0  ,0  , 0,0  ,0  ,219 					
	iBitMapOneSize	= ($ - pBitMapOne)
	pBitMapTwo   	BYTE 219,219,219,219,219,219  
					BYTE 0  ,0  , 0,0  ,0   ,219  
					BYTE 0  ,0  , 0,0  ,0   ,219  
					BYTE 219,219,219,219,219,219  
					BYTE 219,0  , 0,0  ,0  ,   0  
					BYTE 219,0  , 0,0  ,0  ,   0  
					BYTE 219,219,219,219,219,219 					
	iBitMapTwoSize	= ($ - pBitMapTwo)
	pBitMapThree   	BYTE 219,219,219,219,219,219  
					BYTE 0  ,0  , 0,0  ,0   ,219  
					BYTE 0  ,0  , 0,0  ,0   ,219  
					BYTE 219,219,219,219,219,219  
					BYTE   0,0  , 0,0  ,0  , 219  
					BYTE   0,0  , 0,0  ,0  , 219  
					BYTE 219,219,219,219,219,219 					
	iBitMapThreeSize	= ($ - pBitMapThree)
	pBitMapFour   	BYTE 219,  0,  0,  0,  0,219  
					BYTE 219  ,0 , 0,  0,  0,219  
					BYTE 219  ,0 , 0,  0,  0,219  
					BYTE 219,219,219,219,219,219  
					BYTE   0,  0,  0,  0,  0,219  
					BYTE   0,  0,  0,  0,  0,219  
					BYTE   0,  0,  0,  0,  0,219 					
	iBitMapFourSize	= ($ - pBitMapFour)
	pBitMapFive   	BYTE 219,219,219,219,219,219  
					BYTE 219,0  , 0,0  ,0   ,  0  
					BYTE 219,0  , 0,0  ,0   ,  0  
					BYTE 219,219,219,219,219,219  
					BYTE   0,0  , 0,0  ,0  , 219  
					BYTE   0,0  , 0,0  ,0  , 219  
					BYTE 219,219,219,219,219,219 					
	iBitMapFiveSize	= ($ - pBitMapFive)
	pBitMapSix  	BYTE 219,219,219,219,219,219  
					BYTE 219,0  , 0,0  ,0   ,  0  
					BYTE 219,0  , 0,0  ,0   ,  0  
					BYTE 219,219,219,219,219,219  
					BYTE 219,0  , 0,0  ,0  , 219  
					BYTE 219,0  , 0,0  ,0  , 219  
					BYTE 219,219,219,219,219,219 					
	iBitMapSixSize	= ($ - pBitMapSix)
	pBitMapSeven   	BYTE  0,219,219,219,219,219
					BYTE  0,0  ,0  ,0  ,0  ,219 
					BYTE  0,0  ,0  ,0  ,0  ,219  
					BYTE  0,0  ,0  ,0  ,0  ,219  
					BYTE  0,0  ,0  ,0  ,0  ,219  
					BYTE  0,0  ,0  ,0  ,0  ,219  
					BYTE  0,0  ,0  ,0  ,0  ,219 					
	iBitMapSevenSize	= ($ - pBitMapSeven)
	pBitMapEight   	BYTE 219,219,219,219,219,219
					BYTE 219,0  , 0,0  ,0   ,219  
					BYTE 219,0  , 0,0  ,0   ,219  
					BYTE 219,219,219,219,219,219  
					BYTE 219,0  , 0,0  ,0  , 219  
					BYTE 219,0  , 0,0  ,0  , 219  
					BYTE 219,219,219,219,219,219 					
	iBitMapEightSize	= ($ - pBitMapEight)
	pBitMapNine   	BYTE 219,219,219,219,219,219  
					BYTE 219,0  , 0,0  ,0   ,219  
					BYTE 219,0  , 0,0  ,0   ,219  
					BYTE 219,219,219,219,219,219  
					BYTE   0,0  , 0,0  ,0  , 219  
					BYTE   0,0  , 0,0  ,0  , 219  
					BYTE   0,  0,  0,  0,  0,219 					
	iBitMapNineSize	= ($ - pBitMapNine)
.code
	     cmp isTimer,1   
	     jne SetScore
		 SetTimer1:
			mov ax,CONSOLE_WIDTH   ;full console width
			sub ax,14	           ;Subtract timer width x2 for tens digit
			mov [pCursorPos.x],ax
			mov [pCursorPos.y],1
			jmp StartTensPlace
		 SetScore:             
			mov [pCursorPos.x],1   ;score positioned in the
			mov [pCursorPos.y],1   ;top left corner for tens digit
 StartTensPlace:
	movzx eax,numToPrint
	mov bl,10
	DIV bl
	movzx ebx,ah ;remainder (one's place)
	movzx eax,al ;quotient (ten's place)
	push ebx

	checkTens:
	cmp eax,9
		je PrintNinety
	cmp eax,8
		je PrintEighty
	cmp eax,7
		je PrintSeventy
	cmp eax,6
		je PrintSixty
	cmp eax,5
		je PrintFifty
	cmp eax,4
		je PrintFourty
	cmp eax,3
		je PrintThirty
	cmp eax,2
		je PrintTwenty
	cmp eax,1
		je PrintTen
		jmp PrintZeros
	PrintZeros:   ;  0_ for 0-9
		mov ebx,OFFSET [pBitMapZero]
		jmp writeTensToScreen
	PrintTen:     ;  1_ for 10-19
		mov ebx,OFFSET [pBitMapOne]
		jmp writeTensToScreen
	PrintTwenty:  ;  2_ for 20-29
		mov ebx,OFFSET [pBitMapTwo]
		jmp writeTensToScreen
	PrintThirty:  ;  3_ for 30-39
		mov ebx,OFFSET [pBitMapThree]
		jmp writeTensToScreen
	PrintFourty:  ;  4_ for 40-49
		mov ebx,OFFSET [pBitMapFour]
		jmp writeTensToScreen
	PrintFifty:   ;  5_ for 50-59
		mov ebx,OFFSET[pBitMapFive]
		jmp writeTensToScreen
	PrintSixty:   ;  6_ for 60-69
		mov ebx,OFFSET[pBitMapSix]
		jmp writeTensToScreen
	PrintSeventy: ;  7_ for 70-79
		mov ebx,OFFSET[pBitMapSeven]
		jmp writeTensToScreen
	PrintEighty:  ;  8_ for 80-89
		mov ebx,OFFSET[pBitMapEight]
		jmp writeTensToScreen
	PrintNinety:  ;  9_ for 90-99
		mov ebx,OFFSET[pBitMapNine]
		jmp writeTensToScreen

	writeTensToScreen:   ;tens place
		mov esi,0
		mov ecx,[iBitMapRows]
		PrintBitMapTens:
			push ecx
			INVOKE SetConsoleCursorPosition, m_hConsoleOutput, DWORD PTR pCursorPos
			INVOKE WriteConsole, m_hConsoleOutput, ADDR [ebx + esi], iBitMapColumns, ADDR iCount, NULL
			pop ecx	
			add esi,iBitMapColumns		;Increase the offset by the number of columns (which will be the start of the next row)
			inc pCursorPos.y
		loop PrintBitMapTens

	checkOnes:
    mov ax,CONSOLE_WIDTH   ;full console width
	sub ax,7	           ;Subtract timer width for ones digit
	cmp isTimer,1
	jne SetScore2
	jmp SetTimer2
  SetTimer2:
	mov ax,CONSOLE_WIDTH   ;full console width
	sub ax,7	           ;Subtract timer width x2 for tens digit
	mov [pCursorPos.x],ax
	mov [pCursorPos.y],1
	jmp StartOnesPlace
  SetScore2:
	mov [pCursorPos.x],8
	mov [pCursorPos.y],1
 StartOnesPlace:
		pop ebx
		mov eax,ebx
		cmp eax,9
		je PrintNine
		cmp eax,8
		je PrintEight
		cmp eax,7
		je PrintSeven
		cmp eax,6
		je PrintSix
		cmp eax,5
		je PrintFive
		cmp eax,4
		je PrintFour
		cmp eax,3
		je PrintThree
		cmp eax,2
		je PrintTwo
		cmp eax,1
		je PrintOne
		jmp PrintZero

	PrintOne:
		mov ebx,OFFSET [pBitMapOne]
		jmp writeOnesToScreen
	PrintTwo:
		mov ebx,OFFSET [pBitMapTwo]
		jmp writeOnesToScreen
	PrintThree:
		mov ebx,OFFSET [pBitMapThree]
		jmp writeOnesToScreen
	PrintFour:
		mov ebx,OFFSET [pBitMapFour]
		jmp writeOnesToScreen
	PrintFive:
		mov ebx,OFFSET [pBitMapFive]
		jmp writeOnesToScreen
	PrintSix:
		mov ebx,OFFSET [pBitMapSix]
		jmp writeOnesToScreen
	PrintSeven:
		mov ebx,OFFSET [pBitMapSeven]
		jmp writeOnesToScreen
	PrintEight:
		mov ebx,OFFSET [pBitMapEight]
		jmp writeOnesToScreen
	PrintNine:
		mov ebx,OFFSET [pBitMapNine]
		jmp writeOnesToScreen
	PrintZero:
		mov ebx,OFFSET [pBitMapZero]
		jmp writeOnesToScreen
	writeOnesToScreen:                   ;ones place
		mov esi,0
		mov ecx,[iBitMapRows]
		PrintBitMapOnes:
			push ecx
			INVOKE SetConsoleCursorPosition, m_hConsoleOutput, DWORD PTR pCursorPos
			INVOKE WriteConsole, m_hConsoleOutput, ADDR [ebx + esi], iBitMapColumns, ADDR iCount, NULL
			pop ecx
		
			add esi,iBitMapColumns		;Increase the offset by the number of columns (which will be the start of the next row)
			inc pCursorPos.y
		loop PrintBitMapOnes
	ret
PrintNumber ENDP

DrawOutro PROC USES eax ebx ecx esi, 
m_hConsoleOutput:HANDLE
LOCAL pCursorPos:COORD, iCount:DWORD
;Draws the intro screen to the passed output handle
.data
pBitMapGame         BYTE 219,219,219,219,0  ,0  ,0  ,219,219,0  ,0  ,0  ,219,0  ,0  ,0  ,219,0  ,0  ,219,219,219,219,0
					BYTE 219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,219,0  ,219,219,0  ,0  ,219,0  ,0  ,0  ,0 
					BYTE 219,0  ,0  ,0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,219,0  ,219,0  ,0  ,219,219,219,0  ,0 
					BYTE 219,0  ,219,219,0  ,0  ,219,219,219,219,0  ,0  ,219,0  ,0  ,0  ,219,0  ,0  ,219,219,219,0  ,0 
					BYTE 219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,0 
					BYTE 219,219,219,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,219,0  ,0  ,219,219,219,219,0
iBitMapGameSize	= ($ - pBitMapGame)
iBitMapGameColumns = LENGTHOF pBitMapGame
iBitMapGameRows	= iBitMapGameSize / iBitMapGameColumns

pBitMapOver		    BYTE 0  ,219,219,0  ,0  ,0  ,219,0  ,0  ,0  ,219,0  ,0  ,219,219,219,219,0  ,0  ,219,219,219,0  ,0
					BYTE 219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,0	,0	,219,0	,0  ,219,0
					BYTE 219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,219,0  ,0  ,219,219,219,0  ,0	,0	,219,0	,0	,219,0
					BYTE 219,0  ,0  ,219,0  ,0  ,219,0  ,0  ,0  ,219,0  ,0  ,219,219,219,0  ,0  ,0  ,219,219,219,0	,0
					BYTE 219,0  ,0  ,219,0  ,0  ,0  ,219,0  ,219,0  ,0  ,0  ,219,0  ,0  ,0  ,0	,0	,219,0	,219,0	,0
					BYTE 0  ,219,219,0  ,0  ,0  ,0  ,0  ,219,0  ,0  ,0  ,0  ,219,219,219,219,0	,0	,219,0	,0  ,219,0
iBitMapOverSize	= ($ - pBitMapOver)
iBitMapOverColumns = LENGTHOF pBitMapOver
iBitMapOverRows	= iBitMapOverSize / iBitMapOverColumns

.code
;Dynamically calculate the position to start drawing, centers it in the middle of the window
mov ax,CONSOLE_WIDTH/2	 ;Half of the console width
sub ax,iBitMapBlockColumns/2	;Subtract half of the bitmap width so that the middle of the bitmap is at half console width (center)
mov [pCursorPos.x],ax
mov [pCursorPos.y],10

mov esi,0
mov ecx,[iBitMapGameRows]
PrintBitMapGame:
mov ebx, OFFSET [pBitMapGame]
push ecx
INVOKE SetConsoleCursorPosition, m_hConsoleOutput, DWORD PTR pCursorPos
INVOKE WriteConsole, m_hConsoleOutput, ADDR [ebx + esi], iBitMapGameColumns, ADDR iCount, NULL
pop ecx
add esi,iBitMapGameColumns	 ;Increase the offset by the number of columns (which will be the start of the next row)
inc pCursorPos.y
loop PrintBitMapGame

mov [pCursorPos.y],17
mov esi,0
mov ecx,[iBitMapOverRows]
PrintBitMapOver:
mov ebx, OFFSET [pBitMapOver]
push ecx
INVOKE SetConsoleCursorPosition, m_hConsoleOutput, DWORD PTR pCursorPos
INVOKE WriteConsole, m_hConsoleOutput, ADDR [ebx + esi], iBitMapOverColumns, ADDR iCount, NULL
pop ecx
add esi,iBitMapOverColumns	 ;Increase the offset by the number of columns (which will be the start of the next row)
inc pCursorPos.y
loop PrintBitMapOver




Return 0
DrawOutro ENDP


END