TITLE BlockHunt Main

.686P		; Pentium Pro or later
.MODEL flat, stdcall

OPTION casemap :none

INCLUDE masm32\include\windows.inc
INCLUDE masm32\include\kernel32.inc
INCLUDE masm32\include\user32.inc
INCLUDE masm32\include\masm32.inc
INCLUDE masm32\include\msvcrt.inc
INCLUDE masm32\macros.asm

INCLUDELIB masm32\lib\kernel32.lib
INCLUDELIB masm32\lib\user32.lib
INCLUDELIB masm32\lib\masm32.lib
INCLUDELIB masm32\lib\msvcrt.lib

INCLUDE BlockHunt.inc

PUBLIC main

system PROTO C, :DWORD

.data?
	hConsoleInput HANDLE ? ; handle to console input
	hConsoleOutputActive HANDLE ? ; handle to active console output
	hConsoleOutputBuffer HANDLE ? ; handle to console output buffer
	hConsoleOutputActiveBuffer HANDLE ?

	hWnd HWND ? ; handle to the console window
.code

;------------------------------------------------------------
main PROC
; Main
;------------------------------------------------------------
	; Prepare the console and output handles
	CALL ConsoleInit

	; Seeding masm32's random number generator
	CALL GetTickCount
	INVOKE nseed, eax 
	mov pGame.nPollingTickCount,eax
	add pGame.nPollingTickCount,POLLING_RATE
	mov pGame.nTimerTickCount,eax
	add pGame.nTimerTickCount,TIMER_RATE

	mov pGame.nTotalScore,0
	mov pGame.nLevel,1 ; Starting level
	mov ax,pGame.nLevel
	add ax,BASE_TIMER
	mov pGame.nTimer,ax

	INVOKE DrawIntro, hConsoleInput, hConsoleOutputBuffer
	CALL UpdateOutput

	INVOKE Sleep,1000
	CALL ClearOutput

	jmp EntryPoint

	.WHILE 1
		CALL GetTickCount
		cmp eax,pGame.nPollingTickCount
		jle NoUpdate

		mov pGame.nPollingTickCount,eax
		add pGame.nPollingTickCount,POLLING_RATE

		CALL IsLevelComplete
		cmp eax,1
		jge LevelComplete

		CALL GetTickCount
		cmp eax,pGame.nTimerTickCount
		jl Update ;TIMER_RATE ammount of time has not passed yet, do not decrement the timer

		mov pGame.nTimerTickCount,eax
		add pGame.nTimerTickCount,TIMER_RATE

		dec pGame.nTimer
		cmp pGame.nTimer,0
		jge Update

		; Next level
	LevelComplete:
		mov ax,pGame.nTimer
		add pGame.nTotalScore,ax ; Add any remaining time to the score
		CALL ClearOutput
		inc pGame.nLevel
		mov ax,BASE_TIMER
		add ax,pGame.nLevel
		mov pGame.nTimer,ax ; Set the timer to BASE_TIME + nLevel

	EntryPoint: ; Enter the loop here so that we can pause and draw everything
		INVOKE PrintNumber, pGame.nTimer, 1, hConsoleOutputBuffer
		INVOKE PrintNumber, pGame.nTotalScore, 0, hConsoleOutputBuffer
		INVOKE ClickToContinue, hConsoleInput, hConsoleOutputBuffer, 0
		INVOKE DoLevel, hConsoleOutputBuffer, pGame.nLevel

	Update:
		INVOKE PrintNumber, pGame.nTimer, 1, hConsoleOutputBuffer
		INVOKE PrintNumber, pGame.nTotalScore, 0, hConsoleOutputBuffer
		CALL UpdateOutput

	NoUpdate:
		INVOKE GetClickedBlock, hConsoleInput
		cmp eax,0
		je EndOfLoop

		INVOKE RemoveBlockFromLevel, hConsoleOutputBuffer, al
		inc pGame.nTotalScore
		INVOKE PrintNumber, pGame.nTotalScore, 0, hConsoleOutputBuffer
		CALL UpdateOutput

	EndOfLoop:	
		cmp pGame.nLevel,DESIRED_LEVELS
		jge EndMessage

		INVOKE Sleep, 10 ; Sleep so we don't eat up cpu
	.ENDW

EndMessage:
	CALL ClearOutput
	INVOKE DrawOutro, hConsoleOutputBuffer
	INVOKE PrintNumber, pGame.nTotalScore, 0, hConsoleOutputBuffer
	CALL UpdateOutput

.data
	pPause BYTE "pause",0
.code
	INVOKE system, ADDR pPause
	INVOKE ExitProcess,0
main ENDP

;------------------------------------------------------------
ConsoleInit PROC USES eax
; Initializes the console and the buffers
;------------------------------------------------------------
.data
	lpConsoleTitle BYTE "BlockHunt",0

	lpConsoleCursorInfo CONSOLE_CURSOR_INFO <1,FALSE>
	lpConsoleScreenBufferSize COORD <CONSOLE_WIDTH,CONSOLE_HEIGHT>
	lpConsoleWindow SMALL_RECT <0,0,CONSOLE_WIDTH-1,CONSOLE_HEIGHT-1>

.code
	;Get the handle to console window
	CALL GetConsoleWindow
	mov hWnd, eax

	;Move the window to 0,0 (upper left of the screen)
	;	http://msdn.microsoft.com/en-us/library/windows/desktop/ms633534(v=vs.85).aspx
	;	BOOL WINAPI MoveWindow(
	;	  _In_  HWND hWnd,
	;	  _In_  int X,
	;	  _In_  int Y,
	;	  _In_  int nWidth,
	;	  _In_  int nHeight,
	;	  _In_  BOOL bRepaint
	;	);
	INVOKE MoveWindow, 
		hWnd,
		0,0,
		0,0,
		TRUE

	;Get the console output handle
	INVOKE GetStdHandle, STD_INPUT_HANDLE
	mov [hConsoleInput],eax

	;Get the console output handle
	INVOKE GetStdHandle, STD_OUTPUT_HANDLE
	mov [hConsoleOutputActive],eax

	;Create the console output screen buffers
	;	http://msdn.microsoft.com/en-us/library/windows/desktop/ms682122(v=vs.85).aspx
	;	HANDLE WINAPI CreateConsoleScreenBuffer(
	;	  _In_        DWORD dwDesiredAccess,
	;	  _In_        DWORD dwShareMode,
	;	  _In_opt_    const SECURITY_ATTRIBUTES *lpSecurityAttributes,
	;	  _In_        DWORD dwFlags,
	;	  _Reserved_  LPVOID lpScreenBufferData
	;	);
	INVOKE CreateConsoleScreenBuffer, 
		GENERIC_WRITE Or GENERIC_READ, 
		NULL, 
		NULL, 
		CONSOLE_TEXTMODE_BUFFER, 
		NULL
	mov [hConsoleOutputBuffer],eax
	INVOKE CreateConsoleScreenBuffer, 
		GENERIC_WRITE Or GENERIC_READ, 
		NULL, 
		NULL, 
		CONSOLE_TEXTMODE_BUFFER, 
		NULL
	mov [hConsoleOutputActiveBuffer],eax
			
	;Set the console title
	;	http://msdn.microsoft.com/en-us/library/windows/desktop/ms686050(v=vs.85).aspx
	;	BOOL WINAPI SetConsoleTitle(
	;	  _In_  LPCTSTR lpConsoleTitle
	;	);
	INVOKE SetConsoleTitle, 
		ADDR lpConsoleTitle
	
	;Set the console input handle to accept mouse input
	;	http://msdn.microsoft.com/en-us/library/windows/desktop/ms686033(v=vs.85).aspx
	;	BOOL WINAPI SetConsoleMode(
	;	  _In_  HANDLE hConsoleHandle,
	;	  _In_  DWORD dwMode
	;	);
	INVOKE SetConsoleMode, 
		hConsoleInput, 
		ENABLE_MOUSE_INPUT

	;Set console cursor info. In our case, disable it
	;	http://msdn.microsoft.com/en-us/library/windows/desktop/ms686019(v=vs.85).aspx
	;	BOOL WINAPI SetConsoleCursorInfo(
	;	  _In_  HANDLE hConsoleOutput,
	;	  _In_  const CONSOLE_CURSOR_INFO *lpConsoleCursorInfo
	;	);
	INVOKE SetConsoleCursorInfo,
		hConsoleOutputActive,
		ADDR lpConsoleCursorInfo
	INVOKE SetConsoleCursorInfo,
		hConsoleOutputBuffer,
		ADDR lpConsoleCursorInfo
	INVOKE SetConsoleCursorInfo,
		hConsoleOutputActiveBuffer,
		ADDR lpConsoleCursorInfo

	;Set the screenbuffer size so when we maximize the window, it will be full screen
	;	BOOL WINAPI SetConsoleScreenBufferSize(
	;	  _In_  HANDLE hConsoleOutput,
	;	  _In_  COORD dwSize
	;	);
	INVOKE SetConsoleScreenBufferSize, 
		hConsoleOutputActive,
		DWORD PTR lpConsoleScreenBufferSize
	INVOKE SetConsoleScreenBufferSize, 
		hConsoleOutputBuffer,
		DWORD PTR lpConsoleScreenBufferSize
	INVOKE SetConsoleScreenBufferSize, 
		hConsoleOutputActiveBuffer,
		DWORD PTR lpConsoleScreenBufferSize
				
	;Set the console window to display the whole screenbuffer
	;	BOOL WINAPI SetConsoleWindowInfo(
	;	  _In_  HANDLE hConsoleOutput,
	;	  _In_  BOOL bAbsolute,
	;	  _In_  const SMALL_RECT *lpConsoleWindow
	;	);
	INVOKE SetConsoleWindowInfo, 
		hConsoleOutputActive, 
		TRUE, 
		ADDR lpConsoleWindow
	INVOKE SetConsoleWindowInfo, 
		hConsoleOutputBuffer, 
		TRUE, 
		ADDR lpConsoleWindow
	INVOKE SetConsoleWindowInfo, 
		hConsoleOutputActiveBuffer, 
		TRUE, 
		ADDR lpConsoleWindow

	INVOKE SetConsoleActiveScreenBuffer, hConsoleOutputActive

	Return 0
ConsoleInit ENDP

;------------------------------------------------------------
UpdateOutput PROC USES eax ebx
; Copies the content from the buffer and moves it to the active output
; This prevents flickering and tearing, and allows for vertical sync
; Moves buffer content to active buffer, makes the active buffer visible by 
; swapping it with the active output and setting it to be visible
;------------------------------------------------------------
	LOCAL lpNumberOfCharsWritten:DWORD
.data 
	lpBuffer CHAR_INFO CONSOLE_WIDTH DUP(CONSOLE_HEIGHT DUP(<>))
	dwBufferSize COORD <CONSOLE_WIDTH,CONSOLE_HEIGHT>
	dwBufferCoord COORD <0,0>
	lpReadRegion SMALL_RECT <0,0,CONSOLE_WIDTH,CONSOLE_HEIGHT>
	lpWriteRegion SMALL_RECT <0,0,CONSOLE_WIDTH,CONSOLE_HEIGHT>
.code
	; Move to the active buffer
	INVOKE ReadConsoleOutput, hConsoleOutputBuffer, ADDR lpBuffer, DWORD PTR dwBufferSize, 
		DWORD PTR dwBufferCoord, ADDR lpReadRegion
	INVOKE WriteConsoleOutput, hConsoleOutputActiveBuffer, ADDR lpBuffer, DWORD PTR dwBufferSize, 
		DWORD PTR dwBufferCoord, ADDR lpWriteRegion
	
	; Make the active buffer visible by swapping it with the active output
	mov eax,hConsoleOutputActive
	mov ebx,hConsoleOutputActiveBuffer
	mov hConsoleOutputActiveBuffer,eax
	mov hConsoleOutputActive,ebx

	INVOKE SetConsoleActiveScreenBuffer, hConsoleOutputActive

	; Clear the active buffer
	INVOKE WriteConsole, hConsoleOutputActiveBuffer, ADDR pBlankChars, LENGTHOF pBlankChars, 
		ADDR lpNumberOfCharsWritten, NULL

	Return 0
UpdateOutput ENDP

;------------------------------------------------------------
ClearOutput PROC
; Clears the output buffer then updates the active output
;------------------------------------------------------------
	LOCAL lpNumberOfCharsWritten:DWORD

	INVOKE WriteConsole, hConsoleOutputBuffer, ADDR pBlankChars, LENGTHOF pBlankChars, ADDR lpNumberOfCharsWritten, NULL
	CALL UpdateOutput
		
	Return 0
ClearOutput ENDP

END main